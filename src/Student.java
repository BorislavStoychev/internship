
public class Student implements Comparable<Object> {

	private String name;
	private int age;
	private String gender;
	private double averageGrade;
	private int entityId;
	
	public Student(String name,int age,String gender,int entityId)
	{
		this.name=name;
		this.age=age;
		this.gender=gender;
		this.averageGrade=Math.random()*4+2;
		this.entityId=entityId;
	}
	public int compareTo(Object obj)
	{
		Student st=(Student)obj;
		if(this.averageGrade>st.averageGrade)
		{
			return -1;
		}
		if(this.averageGrade<st.averageGrade)
		{
			return 1;
		}
		return 0;
	}
	public String getName()
	{
		return this.name;
	}
	public String getGender()
	{
		return this.gender;
	}
	public int getEntityId()
	{
		return this.entityId;
	}
	public double getGrade()
	{
		return this.averageGrade;
	}
	public String toString()
	{
		return "Name: "+this.name+" Age: "+this.age+" Grade: "+String.format("%.2f",this.averageGrade);
	}
	public double calculatePayment(Institution inst)
	{
		if(inst.averageGrade()!=0)
		{
		    return (this.age/inst.averageGrade())*100+inst.getTax();
		}
		return 0;
	}
}
