import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public abstract class Institution {

	private int id;
	private String name;
	private String address;
	private ArrayList<Student>students=new ArrayList<>();
	
	public Institution(int id,String name,String address)
	{
		this.id=id;
		this.name=name;
		this.address=address;
		readFile("students.txt");
	}
	public String getName()
	{
		return this.name;
	}
	public abstract int getTax();
	public Student topStudent()
	{
		Collections.sort(students);
		return students.get(0);
	}
	public Student topStudentWithDifferentGender()
	{
	    Collections.sort(students);
	    Student topStudent=topStudent();
	    for(Student student:students)
	    {
	    	//If this student has different gender than the top student
		   if(!(student.getGender().equals(topStudent.getGender())))
		   {
			  topStudent=student;
			  break;
		   }
	    }
	    //Return the top student with different gender
	    return topStudent;
	  }
	public double averageGrade()
	{
		double grades=0d;
		for(Student student:students)
		{
				grades+=student.getGrade();
		}
		if(students.size()!=0)
		{
			return grades/students.size();
		}
		return 0;
	}
	public double totalIncome(Institution inst)
	{
		double income=0d;
		for(Student student:students)
		{
			income+=student.calculatePayment(inst);
		}
		return income;
	}
	public Student topContributer(Institution inst)
	{
		Student temp=students.get(0);
		for(Student student:students)
		{
			if(student.calculatePayment(inst)>temp.calculatePayment(inst))
			{
				temp=student;
			}
		}
		return temp;
	}
	private void readFile(String filename)
	{
		Scanner sc=null;
		try
		{
			sc=new Scanner(new File(filename));
			while(sc.hasNextLine())
			{
				Student student=new Student(sc.next(),sc.nextInt(),sc.next(),sc.nextInt());
				//Add only if student's entityId is the same as institution's id
				if(student.getEntityId()==this.id)
				{
				   System.out.println("Hello "+student.getName()+" and welcome to "+this.name);
				   students.add(student);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if(sc!=null)
			sc.close();
		}
	}
}
