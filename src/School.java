
public class School extends Institution {

	private final int TAX=50;
	
	public School(int id,String name,String address)
	{
		super(id,name,address);
	}
	@Override
	public int getTax()
	{
		return this.TAX;
	}
}
