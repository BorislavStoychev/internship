import java.util.ArrayList;
import java.util.Collections;

public class Main {

	private static Student topStudent(ArrayList<Institution>inst)
	{
		ArrayList<Student>students=new ArrayList<>();
		//Fill the list with top students from each institution
		for(Institution institution:inst)
		{
			students.add(institution.topStudent());
		}
		Collections.sort(students);
		return students.get(0);
	}
	private static void topStudentsPerGender(ArrayList<Institution>inst)
	{
		ArrayList<Student>students=new ArrayList<>();
		//Fill the list with top students with different gender
		for(Institution institution:inst)
		{
			students.add(institution.topStudent());
			students.add(institution.topStudentWithDifferentGender());
		}
		Collections.sort(students);
		Student topStudent=students.get(0);
		//Print the top student among all institutions
		System.out.println(topStudent);
	    for(Student student:students)
	    {
	    	//Print the first student with different gender
	    	if(!(student.getGender().equals(topStudent.getGender())))
	    	{
	    		System.out.println(student);
	    		break;
	    	}
	    }
	}
	private static double totalIncome(ArrayList<Institution>inst)
	{
		double totalIncome=0;
		for(Institution institution:inst)
		{
			totalIncome+=institution.totalIncome(institution);
		}
		return totalIncome;
	}
	public static void main(String[] args)
	{
		University university1=new University(1,"TU-VARNA","kv.Levski");
		University university2=new University(2,"VVMU","kv.Pobeda");
		School school1=new School(3,"Kliment Ohridski","shishkova gradinka");
		School school2=new School(4,"Petyr Beron","kv.Briz");
		School school3=new School(5,"Elektro","kv.Pobeda");
		ArrayList<Institution>inst=new ArrayList<>();
		inst.add(university1);
		inst.add(university2);
		inst.add(school1);
		inst.add(school2);
		inst.add(school3);
		System.out.println("\nTop student: "+topStudent(inst));
        System.out.println("\nTop students per gender: ");
        topStudentsPerGender(inst);
        System.out.println("\nAverage grade per institution: ");
        for(Institution institution:inst)
		{
			System.out.println(institution.getName()+" Average Grade: "+String.format("%.2f",institution.averageGrade()));
		}
        System.out.println("\nTotal income for all institutions: "+String.format("%.2f",totalIncome(inst)));
        System.out.println("\nTop contributers per institution: ");
        for(Institution institution:inst)
        {
        	System.out.println(institution.getName()+" has a top contributer- "+institution.topContributer(institution));
        }
	}
}
